import Mouton from "./class/Mouton";
import { AnimalSexe } from "./class/AnimalSexe";
import Vache from "./class/Vache";
import { VacheNoire } from "./class/VacheNoire";
import Poule from "./class/Poule";

function randomName() {
  const cons = [
    "b",
    "c",
    "d",
    "f",
    "g",
    "h",
    "j",
    "k",
    "l",
    "m",
    "n",
    "p",
    "q",
    "r",
    "s",
    "t",
    "v",
    "w",
    "x",
    "z",
  ];
  const voy = ["a", "e", "i", "o", "y", "u"];
  let nbSyl = Math.floor(Math.random() * 6) + 1;
  let name: string = "";
  for (let i = 0; i < nbSyl; i++) {
    let syl =
      cons[Math.floor(Math.random() * cons.length)] +
      voy[Math.floor(Math.random() * voy.length)];
    name += syl;
  }
  return name.charAt(0).toUpperCase() + name.slice(1);
}

// Créer Shaun le mouton de 8 ans
let shaun: Mouton = new Mouton("Shaun", 8, "male");
// faire parler Shaun
shaun.parle();
// faire vieillir shaun
shaun.vieillit();
// refaire parler shaun
shaun.parle();
//essayer de faire rajeunir shaun pour qu'il retrouve ses 4 ans
try {
  // avec du code ici
} catch (error) {
  console.log("spoiler ce n'est pas possible");
}
/**
 * Créer le type AnimalSexe qui peut avoir comme valeur soit "Male" soit "Female"
 */

/**
 * Modifier la classe Mouton pour lui ajouter un attribut sexe, privé et de type AnimalSexe
 * Ajouter le choix du sexe dans le constructeur du mouton (valeur par défaut "female")
 * Modifier la méthode parle() du mouton pour qu'il dise "le mouton" ou "la brebis" en fonction du sexe
 */
/**
 * Ajouter la méthode copuler(partenaire: Mouton) à la classe mouton
 * Si le partenaire est du sexe opposé au mouton cette méthode doit renvoyer un nouveau mouton d'age 0
 * dont le nom est composer du nom de sa mere concaténé avec le nom de son père
 * son sexe doit être aléatoire
 */
// créer un mouton Kevin, 9 ans, male
let kévin: Mouton = new Mouton("kévin", 9, "male");

// faire copuler Kevin et Shaun et récupérer l'enfant dans une variable
let agneau: Mouton = kévin.copuler(shaun);
// faire parler l'agneau récupéré
try {
  agneau.parle();
} catch (e) {
  console.log("'agneau' ne peut pas parler parce qu'il n'existe pas");
}

let kiki = new Mouton("kiki", 0);
/**
 * Modifier la méthode copuler(partenaire: Mouton) pour qu'une erreur soit lever si un des deux moutons à moins de 4 ans
 * cf. throw errors
 */
try {
  kiki.copuler(kévin);
} catch (e) {
  console.error("kiki est trop jeune");
  kiki.vieillit();
}
/**
 * créer un tableau troupeau qui contient 50 moutons avec des noms et des ages aléatoires
 * les ages doivent être en 0 et 12
 * 9 moutons sur 10 doivent en fait être des brebis
 */
// création du tableau
let troupeau: Mouton[] = [];
for (let i = 0; i < 50; i++) {
  let nomAleatoire: string = randomName();
  let age012: number = Math.floor(Math.random() * 13);
  let sexe9female: AnimalSexe = i % 10 === 0 ? "male" : "female";
  troupeau.push(new Mouton(nomAleatoire, age012, sexe9female));
}

// .... //
// faire bêler tous les moutons du troupeau
troupeau.forEach((m) => m.parle());
/**
 * Les brebis ne peuvent avoir qu'un agneau par an
 * Faire en sorte en modifiant la classe mouton que la méthode copuler()
 * ne fonctionne pas tant qu'un an (age augmenter de 1)
 */
let newB = shaun.copuler(kévin);

console.assert(newB === undefined, "Le systeme de fécondité ne marche pas");

/**
 * Créer une classe Vache qui fonctionne comme les moutons
 * Puis créer deux vache (un male et une femelle) et faite leur faire plein de petits veaux
 */
let marguerite = new Vache("marguerite", 14);
marguerite.parle();
let johnny = new VacheNoire("johnny", 12, "male");
let bobby = johnny.copuler(marguerite);
console.log(bobby);

/*
        AH MON DIEU C'EST AFFREUX IL Y A PLEIN DE CODE DUPLIQUE ENTRE VACHE ET MOUTON !!!!
 */

/**
 * Créer une classe Animal qui contient tous le code commun entre les Moutons et les Vaches
 * Faire hériter ( class ... extends ....) (rechercher Inheritance sur google...) les classes
 * Mouton et Vache de Animal.
 * Modifier les classes pour n'avoir que ce qui change entre les deux.
 */

/**
 * Créer la classe Poule qui hérite de Animal
 * Créer une classe Oeuf qui hérite aussi de Animal
 * Ajouter une méthode pondre() à la classe Poule qui retourne un nouvel Oeuf d'age 0 quand on l'appelle
 * Ajouter une classe éclore à Oeuf qui retourne une nouvelle Poule.
 */
let cocotte: Poule = new Poule("cocotte", 5, "female");
let coco: Poule = new Poule("coco", 6, "male");

// créer un poulailler qui contient 10 poules.
// faire pondre des oeufs à toutes les poules et les ajouter dans un tableau nid
// faire éclore tous les oeufs et ajouter les poules qui en sortent au poulailler

/**
 * Créer cette classe
 * on peut ajouter et retirer des animaux.
 * S'il n'y a plus de place il faut renvoyer une erreur
 * Si on la vide, ça retourne un tableau qui contient tous les animaux qu'il y a dedans
 * Si on appelle vieillir ça fait vieillir tous les animaux dedans
 * afficher() affiche la liste des animaux (le type, le nom et l'age)
 */
/*

 ┌───────────────────────────────┐
 │           Etable              │
 ├───────────────────────────────┤
 │ -nbPlaceMax:Number            │
 │ -animaux:Animal[]             │
 ├───────────────────────────────┤
 │ ajouter(animal:Animal)        │
 │ retirer(animal:Animal):Animal │
 │ vider():Animal[]              │
 │ vieillir()                    │
 │ afficher()                    │
 └───────────────────────────────┘

 */
// pour afficher le type (la classe) d'un objet
// mon_objet.constructor.toString().match(/\w+/g)[1]
// ca ne marche plus quand le code est minifié donc ne pas trop s'y fier dans la vrai vie
console.log(shaun.constructor.toString().match(/\w+/g)[1]);

// créer une étable pour y mettre toutes les Vaches
// Une autre avec tous les Moutons

/**
 * Créer une classe poulailler qui hérite de Etable
 * La différence c'est que seules des poules peuvent être mises dedans.
 * Ajouter une méthode pondre() qui fait pondre toutes les poules
 * Quand les poules pondes il faut conserver les oeufs dans un paramettre
 *    -nid:Oeuf[]
 * Ajouter une méthode AllerChercherLesOeufs(n):Oeuf[] qui retourne un tableau d'Oeuf
 * avec dedans autant d'Oeuf que n (autant que possible)
 * Il faut "transférer" des oeufs de nid vers le résultat autant que possible jusqu'à n
 *
 */
// créer un poulailler de 32 places avec 30 poules dedans
// faire pondre les poules
// récolter 20 oeufs
/**
 * Ajouter une méthode éclore() au poulailler qui fait éclore tous les Oeuf du nid
 * Les poules crées doivent être ajoutées au poulailler
 * Penser à supprimer (delete ...) les oeufs qui ont éclos
 * S'il n'y à pas de assez de place, afficher un message pour prévenir "Il Y A TROP DE POULE !!"
 * et retourner les poules surnuméraires dans un tableau
 */
// faire éclore les oeufs du poulailler
// récupérer les poules en trop et les faire caqueter
