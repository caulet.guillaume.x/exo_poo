import Animal from "./Animal";
import { AnimalSexe } from "./AnimalSexe";
import Oeuf from "./Oeuf";

class Poule extends Animal {
  constructor(nom: string, age: number, sexe: AnimalSexe = "female") {
    super(nom, age, sexe);
    if (this.sexe === "female") {
      this.genre = "la poule";
    } else {
      this.genre = "le coq";
    }
  }
  pondre(partenaire: Poule, oeuf) {
    let nomOeuf = "oeuf" + this.nom + partenaire.nom;
    return new Oeuf(nomOeuf, 0);
  }
}
export default Poule;
