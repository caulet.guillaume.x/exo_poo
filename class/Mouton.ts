import { AnimalSexe } from "./AnimalSexe";
import Animal from "./Animal";

class Mouton extends Animal {
  constructor(nom: string, age: number, sexe: AnimalSexe = "female") {
    super(nom, age, sexe);
    if (this.sexe === "female") {
      this.genre = "la brebis";
    } else {
      this.genre = "le mouton";
    }
  }

  copuler(partenaire: Mouton): Mouton {
    return super.copuler(partenaire, Mouton);
  }
}
export default Mouton;
