type AnimalSexe = "male" | "female";
class Vache {
  // le nom de la classe a.k.a le "type" des moutons
  readonly nom: string; // le nom du mouton. Il peut être lu par les autres
  // objet d'autres classes mais
  // pas modifié (on ne peut pas renommer un mouton)
  private age: number; // l'age du mouton, c'est privé, les autres classe
  // ne peuvent pas savoir ce qu'il y a dedans
  private sexe: AnimalSexe;
  private fecondite: boolean;

  constructor(nom: string, age: number, sexe: AnimalSexe = "female") {
    // permet de créer un nouveau mouton avec 'new'
    this.nom = nom; // par exemple `new Mouton ("kevin", 16) créé
    this.age = age;
    this.sexe = sexe; // un Mouton qui s'appelle Kévin et a 16 ans
  }

  vieillit() {
    // Quand on appelle cette méthode sur un mouton son age
    this.age++;
    this.fecondite = true; // augmente de 1 c'est le seul moyen de modifier l'age
  } // d'un mouton ce qui évite de le faire rajeunir par exemple

  parle() {
    let message: string = "";
    // permet "d'afficher" le mouton dans la console
    if (this.sexe === "male") {
      message =
        // comme elle utilise le nom et l'age le résultat sera
        `Meuuuuh je suis ${this.nom} le taureau et j'ai ${this.age} ans`;
    } else {
      message = `Meuuuuuh je suis ${this.nom} la vache et j'ai ${this.age} ans`;
    }
    console.log(message); // unique pour chaque mouton (à peu près)
    return message;
  }
  copuler(partenaire: Vache) {
    if (this.age > 3 && partenaire.age > 3) {
      if (
        this.sexe != partenaire.sexe &&
        this.fecondite &&
        partenaire.fecondite
      ) {
        let veau = this.nom + partenaire.nom;
        let sexeAleatoire: AnimalSexe = "female";
        if (Math.random() > 0.5) sexeAleatoire = "male";
        this.fecondite = this.sexe === "female" ? false : true;
        partenaire.fecondite = partenaire.sexe === "female" ? false : true;
        return new Vache(veau, 0, sexeAleatoire);
      }
      return undefined;
    }
    throw new Error("Une des vaches est trop jeune");
  }
}
export default Vache;
