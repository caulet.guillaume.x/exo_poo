import Vache from "./Vache";
import { AnimalSexe } from "./AnimalSexe";

export class VacheNoire extends Vache {
  readonly couleur: string = "noire";
}

export class VacheBlanche extends Vache {
  readonly couleur: string = "blanche";

  parle(): string {
    console.log("Meuh");
    return "Meuh";
  }
}
