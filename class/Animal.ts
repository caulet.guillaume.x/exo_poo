import { AnimalSexe } from "./AnimalSexe";

class Animal {
  readonly nom: string;
  protected age: number;
  protected sexe: AnimalSexe;
  protected fecondite: boolean;
  protected genre: string;

  constructor(nom: string, age: number, sexe: AnimalSexe = "female") {
    this.sexe = sexe;
    this.nom = nom;
    this.age = age >= 0 ? age : 0;
    this.fecondite = true;
    if (this.sexe === "female") {
      this.genre = "la femelle";
    } else {
      this.genre = "le male";
    }
  }

  vieillit() {
    this.age++;
    this.fecondite = true;
  }

  parle() {
    let message: string = `je suis ${this.nom} ${this.genre} et j'ai ${this.age} ans`;
    console.log(message);
    return message;
  }
  copuler(partenaire: Animal, typeEnfant) {
    if (this.age > 3 && partenaire.age > 3) {
      if (
        this.sexe !== partenaire.sexe &&
        this.fecondite &&
        partenaire.fecondite
      ) {
        let nomBebe = this.nom + partenaire.nom;
        let sexeAleatoire: AnimalSexe = "female";
        if (Math.random() > 0.5) sexeAleatoire = "male";
        this.fecondite = this.sexe !== "female";
        partenaire.fecondite = partenaire.sexe !== "female";
        return new typeEnfant(nomBebe, 0, sexeAleatoire);
      }
      return undefined;
    }
    throw new Error("Un des animaux est trop jeune");
  }
}
export default Animal;
