import Animal from "./Animal";

export class Etable {
  private animaux: Animal[] = [];
  readonly nbPlaceMax: number;

  constructor(nbPlace: number) {
    this.nbPlaceMax = nbPlace;
  }
  ajouter(animal: Animal) {
    this.animaux.push(animal);
  }
  retirer(animal: Animal): Animal {
    let i = this.animaux.indexOf(animal);
    delete this.animaux[i];
    return animal;
  }
  vider(): Animal[] {}
  vieillir() {
    this.animaux.forEach((a) => a.vieillit());
  }
  afficher() {
    this.animaux.forEach((a) => a.parle());
  }
}
